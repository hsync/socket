﻿// See https://aka.ms/new-console-template for more information
using System.Net.Sockets;
using System.Net;
using System.Text;

Console.WriteLine("Hello, World!");
StartClient();


void StartClient()
{
    byte[] bytes = new byte[1024];

    try
    {
        // Connect to a Remote server
        // Get Host IP Address that is used to establish a connection
        // In this case, we get one IP address of localhost that is IP : 127.0.0.1
        // If a host has multiple addresses, you will get a list of addresses
        IPHostEntry host = Dns.GetHostEntry("localhost");
        IPAddress ipAddress = host.AddressList[0];
        IPEndPoint remoteEP = new IPEndPoint(ipAddress, 11000);

        // Create a TCP/IP  socket.
        Socket sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        try
        {
            // Connect to Remote EndPoint
            sender.Connect(remoteEP);

            Console.WriteLine("Socket connected to {0}", sender.RemoteEndPoint.ToString());


            Task.Run(() =>
            {
                while (true)
                {
                    // Receive the response from the remote device.
                    int bytesRec = sender.Receive(bytes);
                    Console.WriteLine("Echoed test = {0}", Encoding.UTF8.GetString(bytes, 0, bytesRec));
                }
            });

            // Encode the data string into a byte array.

            while (true)
            {
                Console.WriteLine("Enter message: ");
                string message = Console.ReadLine();
                if (message == "exit")
                {
                    break;

                }
                byte[] msg = Encoding.UTF8.GetBytes(message);
                // Send the data through the socket.
                int bytesSent = sender.Send(msg);
            }


            // Release the socket.
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();

        }
        catch (ArgumentNullException ane)
        {
            Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
        }
        catch (SocketException se)
        {
            Console.WriteLine("SocketException : {0}", se.ToString());
        }
        catch (Exception e)
        {
            Console.WriteLine("Unexpected exception : {0}", e.ToString());
        }

    }
    catch (Exception e)
    {
        Console.WriteLine(e.ToString());
    }
}
