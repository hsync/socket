﻿// See https://aka.ms/new-console-template for more information
using System.Net;
using System.Net.Sockets;
using System.Text;

Console.WriteLine("Hello, World!");
StartServer();
void StartServer()
{
    IPHostEntry host = Dns.GetHostEntry("localhost");
    IPAddress ipAddress = host.AddressList[0];
    IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);
    try
    {

        // Create a Socket that will use Tcp protocol
        Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        // A Socket must be associated with an endpoint using the Bind method
        listener.Bind(localEndPoint);
        // Specify how many requests a Socket can listen before it gives Server busy response.
        // We will listen 10 requests at a time
        listener.Listen(10);

        Console.WriteLine("Waiting for a connection...");

        // Incoming data from the client.


        while (true)
        {
            Socket handler = listener.Accept();
            Console.WriteLine($"Socket Connected: {handler.RemoteEndPoint}");
            Task.Run(() =>
            {
                string data = null;
                byte[] bytes = null;
                while (true)
                {
                    bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);
                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                    // Show the data on the console.
                    Console.WriteLine($"{handler.RemoteEndPoint} => {data}");
                    // Echo the data back to the client.
                    byte[] msg = Encoding.UTF8.GetBytes("Message Recived");
                    handler.Send(msg);
                    data = null;
                }

                //handler.Shutdown(SocketShutdown.Both);
                //handler.Close();
            });

        }

    }
    catch (Exception e)
    {
        Console.WriteLine(e.ToString());
    }

    Console.WriteLine("\n Press any key to continue...");
    Console.ReadKey();
}